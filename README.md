# Convert SSH public key from/to RFC4716/OpenSSH format

Examples:

~~~ruby
require 'ssh-publickey'

# Convert key
SSHPublicKey.openssh_to_rfc4716(k_openssh)
SSHPublicKey.rfc4716_to_openssh(k_rfc4716)

# Test key
SSHPublicKey.is_openssh?(k)
SSHPublicKey.is_rfc4716?(k)
~~~
