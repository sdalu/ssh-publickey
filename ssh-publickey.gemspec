# -*- encoding: utf-8 -*-

require_relative 'lib/ssh-publickey/version'

Gem::Specification.new do |s|
    s.name        = 'ssh-publickey'
    s.version     = SSHPublicKey::VERSION
    s.summary     = "SSH public key conversion"
    s.description = "Convert SSH public key from/to RFC4716/OpenSSH format"
    s.authors     = [ "Stéphane D'Alu" ]
    s.email       = [ 'stephane.dalu@insa-lyon.fr' ]
    s.files       = %w[ README.md ssh-publickey.gemspec ] + Dir['lib/**/*.rb']
    s.homepage    = 'https://gitlab.com/sdalu/ssh-publickey'
    s.license     = 'MIT'

    s.add_development_dependency 'yard', '~>0'
    s.add_development_dependency 'rake', '~>13'
end
