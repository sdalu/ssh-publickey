require_relative 'ssh-publickey/version'

# Convert SSH public key from/to RFC4716/OpenSSH format
#
class SSHPublicKey
    TAG_COMMENT    = 'Comment'
    TAG_DIRECTIVES = 'x-Directives'


    @@tag_comment    = TAG_COMMENT.downcase
    @@tag_directives = TAG_DIRECTIVES.downcase
        
    
    # Report public key processing error
    class PublicKeyError < StandardError
    end

    
    # Maximum text size in a line for PEM format
    LINE_BREAK = 70

    
    # Regex for authorized key
    AUTHORIZED_KEY_REGEX =
        /(?:    (?<directives>(?:[!-~]|\s(?=.*"))+) \s+ )?   # Options
                (?<type>      [a-z0-9_-]+)          \s+      # Type
                (?<key>       [A-Za-z0-9+\/]+=*)             # Key
         (?:\s+ (?<comment>.*?)                     \s* )?   # Comments
         /x

    
    # Regex for OpenSSH public key
    # Same as authorized keys but without directives
    PUBKEY_OPENSSH_REGEX =
        /       (?<type>   [a-z0-9_-]+)             \s+      # Type
                (?<key>    [A-Za-z0-9+\/]+=*)                # Key
         (?:\s+ (?<comment>.*?)                     \s* )?   # Comments
         /x

    
    # Regex for RFC4716 public key
    PUBKEY_RFC4716_REGEX =
        /----\sBEGIN\sSSH2\sPUBLIC\sKEY\s----\R
         (?<tags> (?:[^:\p{Space}\p{Cntrl}]{1,64}            # Tags
                     \s*:\s*
                     (?:[^\\\r\n]*\\\R)* [^\\\r\n]+\R)*)
         (?<key>  (?:[A-Za-z0-9+\/]+\R)*                     # Key
                  [A-Za-z0-9+\/]+=*\R)
         ----\sEND\sSSH2\sPUBLIC\sKEY\s----
         /xmu

    
    # Regex for one only key
    PUBKEY_RFC4716_REGEX_1 = /\A#{PUBKEY_RFC4716_REGEX}\R?\z/ # :nodoc:
    PUBKEY_OPENSSH_REGEX_1 = /\A#{PUBKEY_OPENSSH_REGEX}\R?\z/ # :nodoc:
    AUTHORIZED_KEY_REGEX_1 = /\A#{AUTHORIZED_KEY_REGEX}\R?\z/ # :nodoc:


    # Test if a public key is in OpenSSH format
    #
    # @param  [String] pubkey   public key in RFC4716 format
    #
    # @return [Boolean]
    #
    def self.is_openssh?(pubkey)
        PUBKEY_OPENSSH_REGEX_1.match?(pubkey)
    end


    # Test if a public key is in RFC4716 format
    #
    # @param  [String] pubkey   public key in RFC4716 format
    #
    # @return [Boolean]
    #
    def self.is_rfc4716?(pubkey)
        PUBKEY_RFC4716_REGEX_1.match?(pubkey)
    end


    # Test if it is a public key (OpenSSH or RFC4716 format)
    #
    # @param  [String] pubkey   public key in RFC4716 format
    #
    # @return [Boolean]
    #
    def self.is_pubkey?(pubkey)
        self.is_openssh?(pubkey) || self.is_rfc4716?(pubkey)
    end


    # Convert a public key in OpenSSH format to RFC4716 format
    #
    # @param  [String] pubkey   public key in OpenSSH format
    #
    # @return [String] public key in RFC4716 format
    #
    def self.openssh_to_rfc4716(pubkey)
        data = self.parse_openssh(pubkey) ||
               raise(PublicKeyError, "invalid OpenSSH public key")
        self.build_rfc4716(*data)
    end


    # Convert a public key in RFC4716 format to OpenSSH format
    #
    # @param  [String] pubkey   public key in RFC4716 format
    #
    # @return [String] public key in OpenSSH format
    #
    def self.rfc4716_to_openssh(pubkey)
        data = self.parse_rfc4716(pubkey) ||
               raise(PublicKeyError, "invalid RFC4716 public key")
        self.build_openssh(*data)
    end


    # Convert a public key to OpenSSH format
    #
    # @param  [String] pubkey   public key
    #
    # @return [String] public key in OpenSSH format
    #
    def self.to_openssh(pubkey)
        self.convert(pubkey, :openssh)
    end


    # Convert a public key to RFC4716 format
    #
    # @param  [String] pubkey   public key
    #
    # @return [String] public key in RFC4716 format
    #
    def self.to_rfc4716(pubkey)
        self.convert(pubkey, :rfc4716)
    end

    # Decompose a public key
    #
    # @param  [String] pubkey   public key
    #
    # @return [String,String,Hash] type, key and headers
    #   
    def self.decompose(pubkey)
        self.parse_openssh(pubkey) ||
        self.parse_rfc4716(pubkey) ||
        raise(PublicKeyError, "invalid public key")
    end

    private


    PUBKEY_RFC4716_HEADERTAG_REGEX =
        /[^:\p{Space}\p{Cntrl}]{1,64}
            \s*:\s*
            (?:[^\\\r\n]*\\\R)*[^\\\r\n]+\R/xmu


    def self.convert(pubkey, to)
        data = self.decompose(pubkey)
        case to
        when :openssh then self.build_openssh(*data)
        when :rfc4716 then self.build_rfc4716(*data)
        else raise ArgumentError
        end
    end


    def self.parse_rfc4716(pubkey)
        unless m = PUBKEY_RFC4716_REGEX_1.match(pubkey)
            return nil
        end

        key        = m[:key].gsub(/\R/, '')
        keydata    = m[:key].unpack1('m')
        len        = keydata.unpack1('N')
        type       = keydata[4,len]
        headers    = Hash[m[:tags].scan(PUBKEY_RFC4716_HEADERTAG_REGEX)
                         .map {|tag| tag.gsub(/\\\R/, '').strip }
                         .map {|tag| tag.split(/\s*:\s*/, 2)    }]

        [ type, key, headers ]
    end


    def self.parse_openssh(pubkey)
        unless m = AUTHORIZED_KEY_REGEX_1.match(pubkey)
            return nil
        end

        type    = m[:type]
        key     = m[:key]
        headers = { TAG_COMMENT    => m[:comment],
                    TAG_DIRECTIVES => m[:directives],
                  }.compact

        [ type, key, headers ]
    end


    def self.build_rfc4716(type, key, headers = {})
        headers = headers.map {|tag, value|
            # Lines are limited to 72 8-bytes char
            #  - limit ourselves to 70 to keep room for \\ and \n
            #  - comment part can be unicode so 1 char can be more
            #    that 1 byte
            linesize = 0
            "#{tag}: #{value}".each_char.slice_before {|c|
                bytesize = c.bytes.size
                if linesize + bytesize > LINE_BREAK
                then linesize  = 0        ; true
                else linesize += bytesize ; false
                end
            }.map(&:join).join("\\\n")
        }

        reskey   = []
        reskey   << "---- BEGIN SSH2 PUBLIC KEY ----"
        reskey   += headers
        reskey   << key.scan(/.{1,70}/).join("\n")
        reskey   << "---- END SSH2 PUBLIC KEY ----"
        reskey.join("\n")
    end
    

    def self.build_openssh(type, key, headers = {})
        normalized_headers = headers.transform_keys {|k| k.downcase }
        directives = normalized_headers[@@tag_directives]
        comment    = normalized_headers[@@tag_comment]
        comment    = comment[1..-2] if !comment.nil?      &&
                                       comment.size >= 2  &&
                                       comment[ 0] == '"' &&
                                       comment[-1] == '"'
        
        [ directives, type,  key, comment ].compact.join(' ')
    end
end
